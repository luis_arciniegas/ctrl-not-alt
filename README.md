# Ctrl not Alt

The extension is compatible with STG and other extensions what work with hidden tabs.

## Credits
 - Logo from [Blake Patterson](https://www.flickr.com/people/blakespot)'s [picture](https://www.flickr.com/photos/blakespot/8406528334) of the Model M keyboard. <img src="https://creativecommons.org/images/deed/svg/cc_blue.svg" alt="Creative Commons" width="18"/> <img src="https://creativecommons.org/images/deed/svg/attribution_icon_blue.svg" alt="Attribution" width="18"/>

## About jtagcat
## About jtagcat
### Firefox Add-Ons:
 - [Ctrl not Alt](https://gitlab.com/jtagcat/ctrl-not-alt/)
 - [Copy Window URLs](https://gitlab.com/jtagcat/copy-window-urls/)
 - [Tab Numbering](https://gitlab.com/jtagcat/tab-numbering)

#### Universal features:
 - All Add-ons are compatible with [<img width="18" src="https://rawgit.com/Drive4ik/simple-tab-groups/master/addon/src/icons/icon.svg" alt="Simple Tab Groups"> STG](https://github.com/Drive4ik/simple-tab-groups/).

### Fund the development
 - Flattr: [@jtagcat](https://flattr.com/@jtagcat)
 - [Stellar](https://rasmus.kall.as/ffaddons-stellar.png): `GAIH6QRNVCPA4BIKYE3L7GUPE6NMX2G2KREIVJAARRGBQJ56JFJZL5GL`
 - [Monero](https://rasmus.kall.as/ffaddons-monero.png): `44HDwJggL5GjcHvXZJ7wJi6f2mxeDg9P2SdixtWA9UjD6SGo13QgZ4J2LNAnGaKR7DaJ96wZgU34s8piF2KZXG5381wC2CX`

### Contact for casual chat or support:
 - ffaddons-gitlab-public@c7.ee
 - [<img src="https://upload.wikimedia.org/wikipedia/commons/b/bb/Keybase_logo_official.svg" alt="Keybase" height="18"/> jtagcat](https://keybase.io/jtagcat)
 - [<img alt="Matrix" src="https://rasmus.kall.as/matrix-favicon.svg" height="18px"> @jtagcat:mozilla.org](https://matrix.to/#/@jtagcat:mozilla.org)