
browser.commands.onCommand.addListener(async function(command) {
    let tabs = await browser.tabs.query({
        currentWindow: true,
        // pinned: false,
        hidden: false,
    });

    let [, num] = command.split('-'),
        tabToActive = 9 == num ? tabs.pop() : tabs[num - 1];

    if (tabToActive) {
        browser.tabs.update(tabToActive.id, {
            active: true,
        });
    }
});
